Problem description:

* We have two fields with cars: A and B.
* Capacity of each field is 20.
* Every night (when we do not rent cars) we can move car from one field to another. It costs 20$ per car.
* Every day we can rent a car. For every car we earn 100$. We cannot rent more cars than we have on our field.
* After whole day of rents, some clients come to give cars back (we do not earn anything from it). If there are more cars returned than we can take, the cars disappear.
* Probability is described as Poisson distribution.
* Expected value for rent in field A is \lambda = 3 and in B: \lambda = 4.
* Exprected value for getting car back for field A is \lambda = 3 and for B is \lambda = 2
* Discount factor (\gamma) = 0.9

* Find optimal policy for this problem

____

* You can change all input values in script to change problem
* p_x, p_y - capacity of field A and B
* gamma - discount value (\gamma)
* lambdas - expected value (\lambda) for releveant field (_W - rent, _Z - return)