import numpy as np
from math import exp, factorial

# Copyright (C) 2017 Maciej Dabrowski

def poisson(l, n):
    return (l**n)/factorial(n)*exp(-l)

def create_poisson_table(p, l1):
    poisson_table = np.zeros(p)
    for x in range(0, p):
        poisson_table[x] = poisson(l1, x)

    return poisson_table

def calculate_all_prob_tables():
    global super_table  # Global table with probs after a whole day from every point (x, y)
    super_table = []
    for x in range(0, p_x):
        super_table.append([])
        for y in range(0, p_y):
            super_table[x].append([])

    for x in range(0, p_x):
        for y in range(0, p_y):

            tmp_table = np.zeros((p_x, p_y))

            for xw in range(0, x + 1):
                for yw in range(0, y + 1):

                    # 1) Calculating prob of lending cars
                    s = 0
                    if xw != x and yw != y:
                        s = poisson_table_w_A[xw] * poisson_table_w_B[yw]
                    elif xw == x and yw != y:
                        s = (1 - sum(poisson_table_w_A[:x])) * poisson_table_w_B[yw]
                    elif xw != x and yw == y:
                        s = poisson_table_w_A[xw] * (1 - sum(poisson_table_w_B[:y]))
                    elif xw == x and yw == y:
                        s = (1 - sum(poisson_table_w_A[:x])) * (1 - sum(poisson_table_w_B[:y]))

                    # 2) Calculating prob of giving cars back
                    for xz in range(0, p_x - x + xw):
                        for yz in range(0, p_y - y + yw):
                            s2 = s
                            shift_x = x - xw + xz
                            shift_y = y - yw + yz
                            if shift_x != p_x - 1 and shift_y != p_y - 1:
                                s2 *= poisson_table_z_A[xz] * poisson_table_z_B[yz]
                            elif shift_x == p_x - 1 and shift_y != p_y - 1:
                                s2 *= (1 - sum(poisson_table_z_A[:xz])) * poisson_table_z_B[yz]
                            elif shift_x != p_x - 1 and shift_y == p_y - 1:
                                s2 *= poisson_table_z_A[xz] * (1 - sum(poisson_table_z_B[:yz]))
                            elif shift_x == p_x - 1 and shift_y == p_y - 1:
                                s2 *= (1 - sum(poisson_table_z_A[:xz])) * (1 - sum(poisson_table_z_B[:yz]))

                            tmp_table[shift_x][shift_y] += s2

            super_table[x][y] = tmp_table


def choose_best_action():
    tmp_table = np.zeros((2, p_x, p_y))

    for x in range(0, p_x):
        for y in range(0, p_y):

            actual_best = 0
            best_move = 0

            for tx in range(0, 6):
                if x >= tx and y + tx < p_y:
                    s = super_table[x - tx][y + tx]
                    r = r_table[x - tx][y + tx]
                    s2 = sum(sum((r + gamma * u_table) * s))
                    if s2 + (-20.0) * tx > actual_best:
                        actual_best = s2 + (-20.0) * tx
                        best_move = tx
                else:
                    break

            for ty in range(1, 6):
                if y >= ty and x + ty < p_x:
                    s = super_table[x + ty][y - ty]
                    r = r_table[x + ty][y - ty]
                    s2 = sum(sum((r + gamma * u_table) * s))
                    if s2 + (-20.0) * ty > actual_best:
                        actual_best = s2 + (-20.0) * ty
                        best_move = -ty
                else:
                    break

            tmp_table[MOVE][x][y] = best_move
            tmp_table[REWARD][x][y] = actual_best

    return tmp_table

def count_prob(tabA, tabB, x, y):

    income = 0

    for x2 in range(0, x+1):
        for y2 in range(0, y+1):
            if x2 != x and y2 != y:
                t = tabA[x2] * tabB[y2]
                income += t * (x2 + y2)
            elif x2 == x and y2!= y:
                t = (1-sum(tabA[:x])) * tabB[y2]
                income += t * (x2 + y2)
            elif y2 == y and x2 != x:
                t = (1-sum(tabB[:y])) * tabA[x2]
                income += t * (x2 + y2)
            elif x2 == x and y2 == y:
                t = (1-sum(tabB[:y])) * (1 - sum(tabA[:x]))
                income += t * (x2 + y2)

    return income

# --- Globals ---

A = 0
B = 1

MOVE = 0
REWARD = 1

# Capacity of fields
p_x = 21
p_y = 21

gamma = 0.9

# _W - rent
# _Z - return
lambdas = {"AW": 3, "AZ": 3, "BW": 4, "BZ": 2}

poisson_table_w_A = create_poisson_table(p_x, lambdas["AW"])
poisson_table_w_B = create_poisson_table(p_y, lambdas["BW"])
poisson_table_z_A = create_poisson_table(p_x, lambdas["AZ"])
poisson_table_z_B = create_poisson_table(p_y, lambdas["BZ"])

def main():
    calculate_all_prob_tables()
    global u_table
    u_table = np.zeros((p_x, p_y))
    global r_table
    r_table = np.zeros((p_x, p_y))
    global move_table
    move_table = np.zeros((p_x, p_y))

    for x in range(0, p_x):
        for y in range(0, p_y):
            r_table[x][y] = count_prob(poisson_table_w_A, poisson_table_w_B, x, y) * 100.0

    u_table = r_table.copy()

    new_table = u_table.copy()
    i = 0
    while True:
        tab = choose_best_action()

        for x in range(0, p_x):
            for y in range(0, p_y):
                v = int(abs(tab[MOVE][x][y]))
        u_table = tab[REWARD]
        move_table = tab[MOVE]
        if np.array_equal(u_table, new_table):
            break
        else:
            new_table = u_table.copy()
            i += 1

    for i in range(0, p_x):
        for j in range(0, p_y):
            print("{:2d} ".format(move_table[i][j].astype(int)), end='')
        print()

if __name__ == "__main__":
    main()